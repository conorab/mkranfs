# mkranfs

Script made in 2017 to create a LUKS-encrypted filesystem with a random key on demand. Useful for when you need to temporarily store sensitive data that is too large to fit on a RAM disk. This script is no longer maintained.
